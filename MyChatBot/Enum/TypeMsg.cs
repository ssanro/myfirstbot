﻿
namespace MyChatBot.Enum
{
    public enum TypeMsg
    {
        Welcome = 1,
        PNR = 2,
        Airport = 3,
        Voucher = 4,
        DataPNR = 5
    }
}