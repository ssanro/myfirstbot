﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using MyChatBot.Enum;
using System.Collections.Generic;

namespace MyChatBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        private TypeMsg step = TypeMsg.Welcome;
        private string PNR = "PNR";
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result;

            if(step.ToString() == PNR)
            {
                PNR = ((Activity)activity).Text.ToString();
            }


            switch (step)
            {
                case TypeMsg.Welcome:
                    await context.PostAsync($"Hello, where is your PNR?");
                    step = TypeMsg.PNR;
                    break;
                case TypeMsg.PNR:
                    List<string> locOptions = new List<string>();
                    locOptions.Add("Madrid");
                    locOptions.Add("Barcelona");

                    PromptDialog.Choice(context, this.OnOptionSelected, locOptions, "In which airport is it located? ", "Not a valid option", 3);

                    step = TypeMsg.Welcome;
                    break;
                default:

                    await context.PostAsync("Invalid operation.");
                    step = TypeMsg.Welcome;
                    break;
            }

        }

        private async Task OnOptionSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                string optionSelected = await result;
                var message = context.MakeMessage();
                var attachment = GetReceiptCard(optionSelected, PNR);

                message.Attachments.Add(attachment);

                await context.PostAsync(message);
            }
            catch (TooManyAttemptsException ex)
            {
                await context.PostAsync($"Ooops! Too many attemps :(. But don't worry, I'm handling that exception and you can try again!");

                context.Wait(this.MessageReceivedAsync);
            }
            finally
            {
                context.Wait(MessageReceivedAsync);
            }
        }

        private static Attachment GetReceiptCard(string optionSelected, string PNR)
        {
            var receiptCard = new ReceiptCard
            {
                Title = "Information PNR",
                Facts = new List<Fact> { new Fact("PNR", PNR), new Fact("Departure", optionSelected), new Fact("Arrive", optionSelected) },
                Items = new List<ReceiptItem>
                {
                    //new ReceiptItem("Departure", price: "$ 38.45", quantity: "368", image: new CardImage(url: "https://github.com/amido/azure-vector-icons/raw/master/renders/traffic-manager.png")),
                    //new ReceiptItem("Arrive", price: "$ 45.00", quantity: "720", image: new CardImage(url: "https://github.com/amido/azure-vector-icons/raw/master/renders/cloud-service.png")),
                    new ReceiptItem("Adult", subtitle: "3", text: "1", price: "45.00€", quantity: "720"),
                    new ReceiptItem("Teen", subtitle: "1", text: "1", price: "45.00€", quantity: "720"),
                    new ReceiptItem("Infant", subtitle: "2", text: "1", price: "0€", quantity: "720"),
                    new ReceiptItem("Pet", subtitle: "1", text: "1", price: "30.00€", quantity: "720"),
                },
                Tax = "9.95€",
                Total = "129.95€",
                //Buttons = new List<CardAction>
                //{
                //    new CardAction(
                //        ActionTypes.OpenUrl,
                //        "More information",
                //        "https://account.windowsazure.com/content/6.10.1.38-.8225.160809-1618/aux-pre/images/offer-icon-freetrial.png",
                //        "https://azure.microsoft.com/en-us/pricing/")
                //}
            };

            return receiptCard.ToAttachment();
        }

    }
}